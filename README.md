# Bid 4 Meal
Technical test for Caravelo developed by Miguel Garrido (www.miguel.nz)

## Framework and Technologies
- Vue-cli 3.0: https://github.com/vuejs/vue-cli
- CSS Stylus: http://stylus-lang.com/
- Webpack: http://stylus-lang.com/
- LocalStorage: https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage

#### Project setup
For development purposes you'll need to install the required `node_modules`.
```
npm install
```

#### Compiles and hot-reloads for development
If you want to keep doing some further development:
```
npm run serve
```

#### Compiles and minifies for production
In order to create a new build please use the following command.
```
npm run build
```

## Project Description

The following project is a prototype web application developed for Caravelo. Bid 4 Meal provides a **Meal bidding service** for passangers flying with Caravelo Air. The user can select multiple meals and add the price s/he wishes for the next journey.

As a prototype this web app uses `LocalStorage` to create a session for the user and to handle the data within flights and meals.

### Data entry
To succesfully log in please use one of the following credentials:
- Full Name: **Burt Reynolds**
- Booking Refernce Number: **TST123**

### Error Handling and Testing and other notes
- `SetTimeout` is used on the **Login Page** in order to recreate a real Login workflow.
- You won't be able to log in if your credentials are wrong. See **Data Entry**.
- You won't be able to submit your bid meals if you haven't select one.
- Bid Meals can be removed from the system prior confirmation.
- You'll see some emojis around 😅.

