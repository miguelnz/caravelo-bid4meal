// vue.config.js
const path = require('path')

module.exports = {
  // baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  chainWebpack: config => {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
    const svgRule = config.module.rule('svg');

    // Types
    types.forEach(
      type => addStyleResource(config.module.rule('stylus').oneOf(type))
    )

    // SVG
    svgRule.uses.clear();
    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader');

  },
}

function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/styles.styl'),
      ],
    })
}