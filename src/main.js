import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import Index from './Index.vue'

import router from './router'
import store from './store'
import './registerServiceWorker'

import moment from 'moment';
Object.defineProperty(Vue.prototype, '$moment', { value: moment });

Vue.config.productionTip = false

Vue.use(VueAxios, axios)

new Vue({
  router,
  store,
  render: h => h(Index)
}).$mount('#app')

// Date - Moment Filter
Vue.filter('getDate',
  function(value){
    if (!value) return ''
    return moment(value).format('MMMM DD, YYYY');
})

Vue.filter('getTime',
  function(value){
    if (!value) return ''
    return moment(value).format('HH:mm:ss');
})